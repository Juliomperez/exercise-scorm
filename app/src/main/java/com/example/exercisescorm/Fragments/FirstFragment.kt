package com.example.exercisescorm.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.exercisescorm.R

class FirstFragment : Fragment(),View.OnClickListener {

    lateinit var btnFragment:Button
    lateinit var txtFragment:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnFragment=view.findViewById(R.id.btnFragment)
        btnFragment.setOnClickListener(this)

        txtFragment=view.findViewById(R.id.txtFragment)
    }

    override fun onClick(v: View?) {


        if (v==btnFragment){
            txtFragment.text="You have press the button"
        }
    }

}