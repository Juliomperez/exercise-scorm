package com.example.exercisescorm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import android.widget.ViewAnimator

class MainActivity : AppCompatActivity() {

    lateinit var etEmail:EditText
    lateinit var etPassword:EditText
    lateinit var txTitle:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun clickLogin(v:View){
        etEmail=findViewById(R.id.etEmail)
        etPassword=findViewById(R.id.etPassword)
        txTitle=findViewById(R.id.txTitle)

        var textEmail:String=etEmail.text.toString()
        var textPassword:String=etPassword.text.toString()

        if (textEmail=="user@ucjc.edu" && textPassword=="123456"){
            Toast.makeText(this,"The user is correct",Toast.LENGTH_LONG).show()
            txTitle.text="Welcome to UCJC"

            var intent: Intent = Intent(baseContext,ExerciseFragment::class.java)
            startActivity(intent)
            this.finish()
        }

        else{
            Toast.makeText(this,"The E-mail or the password are incorrect",Toast.LENGTH_LONG).show()

        }
    }

    fun clickCancel(v:View){
        etEmail.setText("")
        etPassword.setText("")
        txTitle.text="Hello From UCJC"

    }
}